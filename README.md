**LIS4369 Fall 2015 A1**

**Loucil Rubio**

[Visit Personal Website](http://www.rubioloucil.com)



## INTRO TO GIT COMMANDS

1. git init --> Initialize Git by creating an empty repo or by reinitializing an existing repo
2. git status --> List files that have been modified which are not being tracked in Git; displays what have been staged for commit 
3. git add --> Updates index/content by adding recently modified files
4. git commit --> Saves changes
5. git push --> Uploads/"pushes" local files to Git
6. git pull --> Fetches from another repo to merge with another repo or local branch
6. git branch --> Displays list of branches


## SSH Git

1. What files does ssh-keygen produce? Identification and public/private rsa keys
2. Where does this program store these files? ~/.ssh folder


## Visual Studio Installation

![VSinstallation.png](https://bitbucket.org/loucilvr/lis4369_fall15_a1/raw/master/images/VSinstallation.png)
